class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :username
      t.numeric :employee_id

      t.timestamps
    end
  end
end
