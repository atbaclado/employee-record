class User
  include Mongoid::Document
  include ActiveModel::SecurePassword

  field :username, type: String
  field :password_digest, type: String
  field :employee_id, type: BSON::ObjectId
  has_secure_password

  validates :password_digest, length: { minimum: 6, :message => "What" }

  validates_presence_of :username
  validates_presence_of :employee_id
  validates_uniqueness_of :employee_id
  validates_uniqueness_of :username
end
