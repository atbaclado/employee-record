class JobTitleController < ApplicationController
  before_action :set_job_title, only: [:edit, :update, :destroy]

  def index
    @job_titles = JobTitle.all
  end

  def new
    @job_title = JobTitle.new
  end

  def create
    @job_title = JobTitle.new(job_title_params)

    if @job_title.save
      redirect_to job_title_index_path, notice: "The Job Title has been created!" and return
    end
    render 'new'
  end

  def edit
  end

  def update
    if @job_title.update_attributes(job_title_params)
      redirect_to job_title_index_path, notice: 'Job Title has been updated!' and return
    end
    render 'edit'
  end

  def destroy
    @job_title.destroy
    redirect_to job_title_index_path, notice: 'Job Title has been deleted!' and return
  end

  private
    def set_job_title
      @job_title = JobTitle.find(params[:id])
    end

    def job_title_params
      params.require(:job_title).permit(:name)
    end
end
