class Address
  include Mongoid::Document
  field :line, type: String
  field :city, type: String
  field :state, type: String
  field :zip, type: Integer
  field :country, type: String

  embedded_in :employee

  validates :line, :city, :state, :presence => true
end
