class User
  include Mongoid::Document
  include ActiveModel::SecurePassword

  field :username, type: String
  field :password_digest, type: String
  field :employee_id, type: BSON::ObjectId
  has_secure_password

  validates :password_digest, length: { minimum: 6, :message => "What" }

  validates :username, :employee_id, :presence => true
  validates :employee_id, :username, :uniqueness => true
end
