class AddJobTitleRefToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_reference :employees, :job_title, foreign_key: true
  end
end
