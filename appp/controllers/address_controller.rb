class AddressController < ApplicationController
  before_action :set_employee,   only: [:create, :edit, :update, :destroy]

  def index
    @employees = Employee.all
  end

  def new
    @address = Address.new
  end

  def create
    @address = @employee.address.new(address_params)

    if @address.save
      redirect_to address_index_path, notice: "The address has been added!" and return
    end
    render 'new'
  end

  def edit
  end

  def update
    if @address.update_attributes(address_params)
      redirect_to address_index_path, notice: 'The address has been updated!' and return
    end
    render 'edit'
  end

  def destroy
    @address.destroy
    redirect_to address_index_path, notice: 'The address has been deleted!' and return
  end

  private
    def set_employee
      @employee = Employee.find(params[:employee])
    end

    def address_params
      params.require(:address).permit(:line, :city, :state, :country, :employee)
    end
end
