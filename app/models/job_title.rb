class JobTitle
  include Mongoid::Document
  field :name, type: String
  field :start_date, type: Date
  field :end_date, type: Date

  has_and_belongs_to_many :employees, inverse_of: nil

  validates :name, :presence => true
end
