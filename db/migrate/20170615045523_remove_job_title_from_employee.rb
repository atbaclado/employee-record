class RemoveJobTitleFromEmployee < ActiveRecord::Migration[5.1]
  def change
    remove_column :employees, :job_title, :string
  end
end
