class RemoveDepartmentFromEmployee < ActiveRecord::Migration[5.1]
  def change
    remove_column :employees, :department, :string
  end
end
