Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root	'users#home'

  get	'/add_employee',	to: 'employees#new'
  post	'/add_employee',	to: 'employees#create'

  get	'/add_user',		to: 'users#new'
  post	'/add_user',		to: 'users#create'

  get	'/add_department',	to: 'departments#new'
  post	'/add_department',	to: 'departments#create'

  get	'/add_jobtitle',	to: 'job_titles#new'
  post	'/add_jobtitle',	to: 'job_titles#create'

  # resources :users
  # resources :department
  # resources :job_title
end
