class Address
  include Mongoid::Document
  field :line, 			type: String
  field :city, 			type: String
  field :state, 		type: String
  field :country, 		type: String

  embedded_in :employee
end
