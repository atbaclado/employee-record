class Employee
	include Mongoid::Document
	field :first_name, 		type: String
	field :middle_name, 	type: String
	field :last_name, 		type: String
	field :birthday, 		type: Date
	field :job_title_id, 	type: BSON::ObjectId
	field :start_date, 		type: Date
  	field :end_date, 		type: Date
	field :department_id,	type: BSON::ObjectId

	has_and_belongs_to_many		:job_title, autosave: true
	has_one						:department, autosave: true
	embeds_one 					:address, autobuild: true

	validates :first_name, length: { minimum: 2, :message => "What" }, format: { with: /\A[a-zA-Z]+\z/, message: "only allows letters" }
	validates :last_name, length: { minimum: 5, :message => "What" }, format: { with: /\A[a-zA-Z]+\z/, message: "only allows letters" }
end