class Department
  include Mongoid::Document
  field :name, type: String

  # has_many :employees, autosave: true

  validates :name, :presence => true
end
