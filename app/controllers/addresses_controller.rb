class AddressesController < ApplicationController

  def index
    @employees = Employee.all
  end

  def new
    @address = Address.new
  end

  def create
    @employee = Employee.find(params[:employee_id])
    @address = @employee.address.new(address_params)

    if @address.save
      redirect_to addresses_path, notice: "The address has been added!" and return
    end
    render 'new'
  end

  def edit
  end

  def update
    if @address.update_attributes(address_params)
      redirect_to addresses_path, notice: 'The address has been updated!' and return
    end
    render 'edit'
  end

  def destroy
    @address.destroy
    redirect_to addresses_path, notice: 'The address has been deleted!' and return
  end

  private
    def address_params
      params.require(:address).permit(:line, :city, :state, :zip, :country, :employee)
    end
end
