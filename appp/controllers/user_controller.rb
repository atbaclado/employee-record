class UserController < ApplicationController
  before_action :set_user, only: [:edit, :update, :destroy]

  def index
    @users = User.all

  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to user_index_path, notice: "The user has been created!" and return
    end
    render 'new'
  end

  def edit
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(user_params)
      redirect_to user_index_path, notice: 'The user has been updated!' and return
    end

    render 'edit'
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    redirect_to user_index_path, notice: 'The user has been deleted!' and return
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:username, :password, :password_confirmation, :employee_id)
    end
end
