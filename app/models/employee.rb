class Employee
  include Mongoid::Document
  field :first_name, type: String
  field :middle_name, type: String
  field :last_name, type: String
  field :birthday, type: String
  field :department_id, type: BSON::ObjectId

  has_and_belongs_to_many :job_titles
  embeds_many :addresses

  validates :first_name, length: { minimum: 2, :message => "What" }, format: { with: /\A[a-zA-Z]+\z/, message: "only allows letters" }
  validates :last_name, length: { minimum: 5, :message => "What" }, format: { with: /\A[a-zA-Z]+\z/, message: "only allows letters" }
end
